package execute;

import common.csv.CsvFile;
import common.start.Element;
import common.start.LocalVariables;
import common.start.StartParameters;
import common.utils.CaptureScreen;
import common.utils.StartChromeDriver;
import common.utils.Validacao;
import navigation.AdicionarCarrinho;
import navigation.HomeSite;

public class Produto {


    public static void Produto(StartParameters sttp, LocalVariables lv, Element el) throws Exception {

        CsvFile.readFileCsv(sttp);
        String[] tableLine = null;

        boolean firstLine = true;


        while ((tableLine = sttp.getReader().readNext()) != null) {
            sttp.setTableLine(tableLine);

            if (firstLine == true) {
                firstLine = false;
                continue;
            }

            StartChromeDriver.openChromeDriver(sttp);
            sttp.getDriver().get(sttp.getProp().getProperty("url"));
            lv.setRobotMessage("meu primeiro print");
            CaptureScreen.capturePage(sttp, lv);
            HomeSite.pesquisarProduto(lv,sttp, el);
            AdicionarCarrinho.adicionarCarrinho(lv,sttp, el);
            lv.setRobotMessage("meu segundo print");
            CaptureScreen.capturePage(sttp, lv);
            Validacao.validacaoCarrinho(lv, sttp, el);
            sttp.getDriver().quit();
        }
    }
}
