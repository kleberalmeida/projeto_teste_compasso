package common.utils;

import common.start.LocalVariables;
import common.start.StartParameters;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import java.io.File;
import java.io.IOException;

public class CaptureScreen {

    public static void capturePage(StartParameters sttp, LocalVariables lv) throws IOException {

        File file = ((TakesScreenshot)sttp.getDriver()).getScreenshotAs(OutputType.FILE);

        FileUtils.copyFile(file, new File(sttp.getDir2() + "//" + lv.getRobotMessage() + ".jpg"));

        String pathEvidencia = sttp.getDir2() + "//" + lv.getRobotMessage() + ".jpg";

        lv.setPathEvidence(pathEvidencia);
    }
}
